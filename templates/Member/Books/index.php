<?php

/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $books
 */
?>
<div class="books px-10">
    <div class="py-8">
        <div class="bg-cool-gray-100 p-5 rounded-sm border border-cool-gray-200">
            <?= $this->Form->create(null, ['action' => '/member/books/index']) ?>
                <div>
                    <div class="flex">
                        <input type="text" name="query" class="w-full font-thin text-sm rounded-l border-b border-t border-l px-3 py-1 focus:outline-none" placeholder="Type Title or Author">
                        <select name="category" id="" class="w-1/2 font-thin text-sm border px-3 py-1 focus:outline-none">
                            <option value="">Select Category</option>
                            <?php foreach ($categories as $x => $y): ?>
                                <option value="<?= $x ?>"><?= ucfirst($y) ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="submit" class="text-sm cursor-pointer hover:bg-orange px-3 py-1 rounded-r text-white focus:outline-none bg-cool-gray-600" value="Search">
                    </div>
                </div>
            <?= $this->Form->end() ?>
        </div>

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div class="inline-block min-w-full border border-cool-gray-200 overflow-hidden">
                <table class="min-w-full leading-normal">
                    <thead>
                        <tr>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('title') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('author') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('user_id', 'Owner') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                Points
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($books as $book) : ?>
                            <tr>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= h($book->title)  ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= h($book->author)  ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= $book->user->first_name . " " . $book->user->last_name ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">100</td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm flex-inline">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $book->id], ['class' => 'px-2 py-1 mx-1 border border-green-600 rounded-sm hover:bg-green-500 hover:text-white text-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="px-5 py-2 bg-white border-t flex items-center justify-between">
                    <span class="text-xs xs:text-sm text-gray-500">
                        <?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?>
                    </span>
                    <div class="inline-flex">
                        <?= $this->Paginator->prev('Previous') ?>
                        <?= $this->Paginator->next('Next') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>