<div class="bg-cool-gray-100 min-h-screen flex flex-col">
    <div class="container max-w-2xl mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <h1 class="mb-8 text-3xl text-center">Sign up</h1>

            <?= $this->Flash->render() ?>
            <?php echo $this->Form->create($user); ?>
            <div class="flex mb-1 justify-evenly">
                <div class="w-1/2 pr-2">
                    <?php echo $this->Form->input('first_name', ['class' => 'block border border-grey-light w-full p-3 rounded mr-2', 'placeholder' => 'First Name']); ?>
                </div>
                <div  class="w-1/2 pl-2">
                    <?php echo $this->Form->input('last_name', ['class' => 'block border border-grey-light w-full p-3 rounded ml-2', 'placeholder' => 'Last Name']); ?>
                </div>
            </div>

            <?php echo $this->Form->input('email', ['class' => 'block border border-grey-light w-full p-3 rounded mb-4', 'placeholder' => 'Email']); ?>

            <div class="flex mb-1 justify-evenly">
                <div class="w-1/2 pr-2">
                    <?php echo $this->Form->password('password', ['class' => 'block border border-grey-light w-full p-3 rounded mb-4', 'placeholder' => 'Password']); ?>
                </div>
                <div  class="w-1/2 pl-2">
                    <?php echo $this->Form->password('repassword', ['class' => 'block border border-grey-light w-full p-3 rounded mb-4', 'placeholder' => 'Re-password']); ?>
                </div>
            </div>

            <button type="submit" class="w-full text-center py-3 rounded bg-blue-800 text-white hover:bg-green-dark focus:outline-none my-1">Create Account</button>
            <?php echo $this->Form->end(); ?>

            <div class="text-center text-sm text-grey-dark mt-4">
                By signing up, you agree to the
                <a class="no-underline border-b border-grey-dark text-grey-dark" href="#">
                    Terms of Service
                </a> and
                <a class="no-underline border-b border-grey-dark text-grey-dark" href="#">
                    Privacy Policy
                </a>
            </div>
        </div>

        <div class="text-grey-dark mt-6">
            Already have an account?
            <a class="no-underline border-b border-blue text-orange-600" href="/member/login/">
                Log in
            </a>.
        </div>
    </div>
</div>