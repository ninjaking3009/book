<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        Books | Books Review
    </title>
    <link href="<?= $this->Url->image('favicon.png') ?>" type="image/png" rel="icon">
    <link href="<?= $this->Url->image('favicon.png') ?>" type="image/png" rel="shortcut icon">

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css([
        'normalize.min',
        'slick',
        'slick-theme',
        'all.min',
        'light.min',
        'build/style',
        'override',
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body class="font-sans">
    <?= $this->element('frontend/member/header'); ?>
    <?php if ($this->Identity->get('id')) : ?>

        <div class="bg-cool-gray-100 py-10 px-20 min-h-screen flex">
            <?= $this->element('frontend/member/sidebar'); ?>
            <div class="bg-white p-5 flex-1 border border-cool-gray-300 rounded-md">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
    <?php else : ?>
        <?= $this->fetch('content') ?>
    <?php endif; ?>



    <?= $this->element('frontend/home/footer'); ?>


    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <?= $this->Html->script([
        'slick.min',
        'light.min',
        'general',
    ]) ?>
</body>

</html>