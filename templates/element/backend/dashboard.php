<?php if ($userPurchase && !empty($lockPurchase)) : ?>
    <?php
    foreach ($lockPurchase as $k => $v) :
    ?>
        <div class="row">

            <div class="col-md-12">

                <div class="callout callout-warning">

                    <h5>Important Notice!</h5>

                    <p>Your purchase <?= $v->code ?> is already lock. You need to withdraw your earning and purchase new plan to continue using the system. <br>Click here to buy new plan 

                        <?= $this->Html->link('[Bitcoin]', ['action' => 'registerPayment','bitcoin', $v->code]) ?> 
                        <?= $this->Html->link('[e-Wallet]', ['action' => 'registerPayment','ewallet', $v->code]) ?>

                    </p>

                </div>

            </div>

        </div>
    <?php
    endforeach;
    ?>
<?php endif; ?>
<?php if (empty($userPurchase)) : ?>

    <div class="row">

        <div class="col-md-12">

            <div class="callout callout-warning">

                <h5>No Active Plan</h5>

                <p>Buy plan using bitcoin now. <br>

                    <?= $this->Html->link('Click here to buy plan using bitcoin', ['action' => 'registerPayment']) ?>

                </p>

            </div>

        </div>

    </div>

<?php endif; ?>

<?php if (!$this->Identity->get('is_admin')) : ?>

    <div class="row">

        <div class="col-md-12 hide-desktop">

            <div class="callout callout-info">

                <h5>Referal Link</h5>

                <p>

                    <a href="<?= $this->Url->build('/register/' . $this->Identity->get('referal_code')); ?>" class="nav-link"><?= "http://" . $_SERVER['SERVER_NAME'] . "/register/" . $this->Identity->get('referal_code'); ?></a>

                </p>

            </div>

        </div>

    </div>

<?php endif ?>

<div class="row">

    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-success"><i class="fas fa-dollar-sign"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Remaining Balance</span>

                <span class="info-box-number">$<?= round($availableBalance, 2) ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>



    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-success"><i class="fas fa-wallet"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">e-Wallet</span>

                <span class="info-box-number">$<?= round($totalEwallet, 2) ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>



    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-info"><i class="fas fa-dollar-sign"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Total Withdrawal</span>

                <span class="info-box-number">$<?= round($totalWithdrawal, 2) ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>



    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-warning"><i class="fas fa-dollar-sign"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Pending Withdrawal Request</span>

                <span class="info-box-number">$<?= round($pendingTotalWithdrawal, 2) ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>



</div>



<div class="row">

    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-success"><i class="fas fa-rocket"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Total Earnings</span>

                <span class="info-box-number">$<?= round($totalEarning, 2) ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>

    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-info"><i class="far fa-calendar"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Daily Earnings</span>

                <span class="info-box-number">$<?= round($totalDailySum, 2) ?? 0 ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>

    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-info"><i class="fas fa-share"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Sharing Rewards</span>

                <span class="info-box-number">$<?= round($totalSharing, 2) ?? 0 ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>

    <div class="col-md-3 col-sm-6 col-12">

        <div class="info-box">

            <span class="info-box-icon bg-info"><i class="fas fa-crown"></i></span>



            <div class="info-box-content">

                <span class="info-box-text">Royalty Rewards</span>

                <span class="info-box-number">$<?= round($userRoyaltyTotal, 2) ?? 0 ?></span>

            </div>

            <!-- /.info-box-content -->

        </div>

        <!-- /.info-box -->

    </div>

    <!-- /.col -->



</div>





<div class="row">

    <div class="col-md-12">

        <div class="card card-info">

            <div class="card-header">

                <h3 class="card-title">Line Chart</h3>



                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>

                    </button>

                </div>

            </div>

            <div class="card-body">

                <div class="chart">

                    <canvas id="lineChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>

                </div>

            </div>

            <!-- /.card-body -->

        </div>

    </div>

</div>





<div class="row">

    <div class="col-md-6">

        <div class="card card-primary">

            <div class="card-header">

                <h3 class="card-title">Latest Earnings</h3>



                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>

                    </button>

                </div>

                <!-- /.card-tools -->

            </div>

            <!-- /.card-header -->

            <div class="card-body">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th><?= $this->Paginator->sort('amount') ?></th>

                            <th><?= $this->Paginator->sort('type') ?></th>

                            <th><?= $this->Paginator->sort('date_added') ?></th>

                        </tr>

                    </thead>

                    <tbody>

                        <?php foreach ($recentEarnings as $userDailyEarning) : ?>

                            <tr>

                                <td>$<?= h($userDailyEarning->amount) ?></td>

                                <td><?= h($userDailyEarning->type) ?></td>

                                <td><?= h($userDailyEarning->date_added) ?></td>

                            </tr>

                        <?php endforeach; ?>

                    </tbody>

                </table>

            </div>

            <!-- /.card-body -->

        </div>

        <!-- /.card -->

    </div>

    <div class="col-md-6">

        <div class="card card-warning">

            <div class="card-header">

                <h3 class="card-title">Latest Referral</h3>



                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>

                    </button>

                </div>

                <!-- /.card-tools -->

            </div>

            <!-- /.card-header -->

            <div class="card-body">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th><?= $this->Paginator->sort('first_name') ?></th>

                            <th><?= $this->Paginator->sort('last_name') ?></th>

                            <th><?= $this->Paginator->sort('status') ?></th>

                            <th><?= $this->Paginator->sort('date_added') ?></th>

                        </tr>

                    </thead>

                    <tbody>

                        <?php foreach ($recentReferral as $user) : ?>

                            <tr>

                                <td><?= h($user->first_name) ?></td>

                                <td><?= h($user->last_name) ?></td>

                                <td><?= h($user->status) ?></td>

                                <td><?= h($user->date_added) ?></td>

                            </tr>

                        <?php endforeach;

                        unset($user) ?>

                    </tbody>

                </table>

            </div>

            <!-- /.card-body -->

        </div>

        <!-- /.card -->

    </div>

</div>



<script>
    $(document).ready(function() {



        var areaChartData = {

            labels: [<?= $chart['date_string'] ?>],

            datasets: [{

                    label: 'Daily Earnings',

                    backgroundColor: 'rgba(60,141,188,0.9)',

                    borderColor: 'rgba(60,141,188,0.8)',

                    pointRadius: false,

                    pointColor: '#3b8bba',

                    pointStrokeColor: 'rgba(60,141,188,1)',

                    pointHighlightFill: '#fff',

                    pointHighlightStroke: 'rgba(60,141,188,1)',

                    data: [<?= $chart['daily'] ?>]

                },

                {

                    label: 'Sharing Rewards',

                    backgroundColor: 'rgba(210, 214, 222, 1)',

                    borderColor: 'rgba(210, 214, 222, 1)',

                    pointRadius: false,

                    pointColor: 'rgba(210, 214, 222, 1)',

                    pointStrokeColor: '#c1c7d1',

                    pointHighlightFill: '#fff',

                    pointHighlightStroke: 'rgba(220,220,220,1)',

                    data: [<?= $chart['sharing'] ?>]

                },

            ]

        }



        var lineChartCanvas = $('#lineChart').get(0).getContext('2d')

        var lineChartOptions = jQuery.extend(true, {}, areaChartOptions)

        var lineChartData = jQuery.extend(true, {}, areaChartData)

        lineChartData.datasets[0].fill = false;

        lineChartData.datasets[1].fill = false;

        lineChartOptions.datasetFill = false



        var lineChart = new Chart(lineChartCanvas, {

            type: 'bar',

            data: lineChartData,

            options: lineChartOptions

        })

    });
</script>