<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.6.0
    </div>
    <strong>Copyright &copy; 2015-<?= date('Y') ?> <a href="#">WynTrade</a>.</strong> All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<?= $this->Html->script('admin/bootstrap.bundle.min') ?>
<!-- AdminLTE App -->
<?= $this->Html->script('admin/adminlte.min') ?>

<?= $this->Html->script('admin/Chart.min') ?>

<?= $this->Html->script('admin/select2.full.min.js') ?>
<?= $this->Html->script('admin/script') ?>
<!-- AdminLTE for demo purposes -->
</body>

</html>