<div class="bg-white w-60 pb-5 mr-10 border border-cool-gray-300 rounded-md">

    <h5 class="text-cool-gray-400 pt-5 px-5 font-semibold mb-3 text-sm">Welcome! <?= ucfirst($this->Identity->get('first_name')) ?></h5>
    <ul class="divide-y divide-gray-200 text-sm">
        <li><a href="#" class="flex px-5 border-l-4 border-orange-400 py-3 hover:bg-cool-gray-100 text-cool-gray-500 items-center">
                <i class="fal fa-home mr-5"></i>
                Home
            </a></li>
        <li x-data="{ isOpen: false }">
            <a href="javascript:;" @click="isOpen = !isOpen" class="border-l-4 px-5 hover:bg-cool-gray-100 border-gray-400 py-3 flex text-cool-gray-500 items-center">
                <i class="fal fa-book mr-5"></i>
                Books
                <i :class="[ isOpen ? 'fa-minus' : 'fa-plus' ]" class="fal block flex-1 text-right"></i>
            </a>
            <ul x-show="isOpen" class="bg-blue-50 border-t-2 border-b-2 border-cool-gray-300">
                <li>
                    <a href="/admin/books/" class="border-l-4 px-5 border-cool-gray-500 py-3 hover:bg-cool-gray-100 flex text-cool-gray-500 items-center">
                        <i class="fal fa-horizontal-rule mr-5"></i>
                        Lists
                    </a>
                </li>
                <li>
                    <a href="/admin/categories/" class="border-l-4 px-5 border-cool-gray-500 py-3 hover:bg-cool-gray-100 flex text-cool-gray-500 items-center">
                        <i class="fal fa-horizontal-rule mr-5"></i>
                        Categories
                    </a>
                </li>
                <li>
                    <a href="/admin/questions/" class="border-l-4 px-5 border-cool-gray-500 py-3 hover:bg-cool-gray-100 flex text-cool-gray-500 items-center">
                        <i class="fal fa-horizontal-rule mr-5"></i>
                        Questions
                    </a>
                </li>
            </ul>
        </li>
        <li><a href="/admin/users/lists" class="border-l-4 px-5 border-gray-400 py-3 flex hover:bg-cool-gray-100 text-cool-gray-500 items-center">
                <i class="fal fa-users mr-5"></i>
                Users
            </a></li>
        <li><a href="/admin/notifications/" class="border-l-4 px-5 border-gray-400 py-3 flex hover:bg-cool-gray-100 text-cool-gray-500 items-center">
                <i class="fal fa-bell mr-5"></i>
                Notifications
            </a></li>
        <li><a href="/admin/user/profile" class="border-l-4 px-5 border-gray-400 py-3 flex hover:bg-cool-gray-100 text-cool-gray-500 items-center">
                <i class="fal fa-user mr-5"></i>
                Profile
            </a></li>
    </ul>

</div>