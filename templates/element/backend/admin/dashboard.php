<div class="row">
    <div class="col-md-6">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Latest Approved Withdrawal</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <?php if ($this->Identity->get('is_admin')) : ?>
                                <th><?= $this->Paginator->sort('user_id') ?></th>
                            <?php endif; ?>
                            <th><?= $this->Paginator->sort('amount') ?></th>
                            <th><?= $this->Paginator->sort('type') ?></th>
                            <th><?= $this->Paginator->sort('date_created') ?></th>
                            <th><?= $this->Paginator->sort('date_approve') ?></th>
                            <th><?= $this->Paginator->sort('status') ?></th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($recentApproveWithdrawal as $userWithdrawal) : ?>
                            <tr>
                                <?php if ($this->Identity->get('is_admin')) : ?>
                                    <td><?= $userWithdrawal->has('user') ? $this->Html->link(ucfirst($userWithdrawal->user->first_name) . " " . ucfirst($userWithdrawal->user->last_name), ['controller' => 'Users', 'action' => 'view', $userWithdrawal->user->id]) : '' ?></td>
                                <?php endif; ?>
                                <td>$<?= h($userWithdrawal->amount) ?></td>
                                <td><?= h($userWithdrawal->type) ?></td>
                                <td><?= h($userWithdrawal->date_created) ?></td>
                                <td><?= h($userWithdrawal->date_approve) ?></td>
                                <td><?= h($userWithdrawal->status) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'UserWithdrawals', 'action' => 'view', $userWithdrawal->id], ['class' => 'btn btn-primary btn-sm']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

    <div class="col-md-6">
        <div class="card card-warning">
            <div class="card-header">
                <h3 class="card-title">Latest Pending Request Withdrawal</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <?php if ($this->Identity->get('is_admin')) : ?>
                                <th><?= $this->Paginator->sort('user_id') ?></th>
                            <?php endif; ?>
                            <th><?= $this->Paginator->sort('amount') ?></th>
                            <th><?= $this->Paginator->sort('type') ?></th>
                            <th><?= $this->Paginator->sort('date_created') ?></th>
                            <th><?= $this->Paginator->sort('date_approve') ?></th>
                            <th><?= $this->Paginator->sort('status') ?></th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($recentPendingWithdrawal as $userWithdrawal) : ?>
                            <tr>
                                <?php if ($this->Identity->get('is_admin')) : ?>
                                    <td><?= $userWithdrawal->has('user') ? $this->Html->link(ucfirst($userWithdrawal->user->first_name) . " " . ucfirst($userWithdrawal->user->last_name), ['controller' => 'Users', 'action' => 'view', $userWithdrawal->user->id]) : '' ?></td>
                                <?php endif; ?>
                                <td>$<?= h($userWithdrawal->amount) ?></td>
                                <td><?= h($userWithdrawal->type) ?></td>
                                <td><?= h($userWithdrawal->date_created) ?></td>
                                <td><?= h($userWithdrawal->date_approve) ?></td>
                                <td><?= h($userWithdrawal->status) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'UserWithdrawals', 'action' => 'view', $userWithdrawal->id], ['class' => 'btn btn-primary btn-sm']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

    <!-- /.card -->
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Latest Registration</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('first_name') ?></th>
                            <th><?= $this->Paginator->sort('last_name') ?></th>
                            <th><?= $this->Paginator->sort('email') ?></th>
                            <th><?= $this->Paginator->sort('status') ?></th>
                            <th><?= $this->Paginator->sort('date_added') ?></th>
                            <th>&nbsp;</th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($latests as $user) : ?>
                            <tr>
                                <td><?= h($user->first_name) ?></td>
                                <td><?= h($user->last_name) ?></td>
                                <td><?= h($user->email) ?></td>
                                <td><?= h($user->status) ?></td>
                                <td><?= h($user->date_added) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['class' => 'btn btn-primary btn-sm']) ?>
                                </td>
                            </tr>
                        <?php endforeach;
                        unset($user) ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-6">
        <div class="card card-warning">
            <div class="card-header">
                <h3 class="card-title">Users Need to Verify</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('first_name') ?></th>
                            <th><?= $this->Paginator->sort('last_name') ?></th>
                            <th><?= $this->Paginator->sort('status') ?></th>
                            <th><?= $this->Paginator->sort('date_added') ?></th>
                            <th>&nbsp;</th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($notverify as $user) : ?>
                            <tr>
                                <td><?= h($user->first_name) ?></td>
                                <td><?= h($user->last_name) ?></td>
                                <td><?= h($user->status) ?></td>
                                <td><?= h($user->date_added) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['class' => 'btn btn-primary btn-sm']) ?>
                                </td>
                            </tr>
                        <?php endforeach;
                        unset($user) ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>