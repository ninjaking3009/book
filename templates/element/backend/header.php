<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WynTrade | System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <?= $this->Html->css('admin/fontawesome.min') ?>

    <!-- Ionicons -->
    <?= $this->Html->css('admin/ionicons.min') ?>

    <!-- overlayScrollbars -->
    <?= $this->Html->css('admin/adminlte.min') ?>

    <!-- Google Font: Source Sans Pro -->
    <?= $this->Html->css('admin/source-sans-pro') ?>

    <!-- Extra Plugins -->
    <?= $this->Html->css('admin/icheck-bootstrap.min') ?>

    <?= $this->Html->css('admin/select2.min') ?>
    <?= $this->Html->css('admin/select2-bootstrap4.min') ?>
    <?= $this->Html->css('admin/custom') ?>

    <!-- jQuery -->
    <?= $this->Html->script('admin/jquery.min') ?>
</head>

<body class="hold-transition ">