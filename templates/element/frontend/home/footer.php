<footer class="border-t border-gray-400">
    <div class="container mx-auto">
        <div class="flex py-10 justify-between">
            <div class="flex-1">
                <div class="text-gray-700 text-base mb-4">Categories</div>
                <ul>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Drama</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Action</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Slice of Life</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Documentary</a>
                    </li>
                </ul>
            </div>
            <div class="flex-1">
                <div class="text-gray-700 text-base mb-4">About</div>
                <ul>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Careers</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Press & News</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Partnerships</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Privacy Policy</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Terms of Services</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Intellectual Property Claims</a>
                    </li>
                </ul>
            </div>
            <div class="flex-1">
                <div class="text-gray-700 text-base mb-4">Support</div>
                <ul>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Careers</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Press & News</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Partnerships</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Privacy Policy</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Terms of Services</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Intellectual Property Claims</a>
                    </li>
                </ul>
            </div>
            <div class="flex-1">
                <div class="text-gray-700 text-base mb-4">About</div>
                <ul>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal">Careers</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Press & News</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Partnerships</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Privacy Policy</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Terms of Services</a>
                    </li>
                    <li class="mb-2">
                        <a href="#" class="text-gray-500 font-normal py-5">Intellectual Property Claims</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>