<header id="main-header" class="w-full">
    <div class="w-full bg-white  px-16 py-5 flex items-center border-b border-gray-400">
        <div class="items-center">
            <a href="/">
                <img class="h-16" src="<?= $this->Url->image('logo.png') ?>" alt="">
            </a>
        </div>
        <div class="ml-10 w-1/4 items-center">
            <?= $this->Form->create(); ?>
            <div class="flex">
                <input type="text" class="w-full font-thin text-sm rounded-l border px-3 py-2 focus:outline-none" placeholder="Search Book">
                <input type="button" class="font-thin  text-sm hover:bg-orange px-3 py-2 rounded-r text-white focus:outline-none bg-blue-800" value="Search">
            </div>
            <?= $this->Form->end(); ?>
        </div>
        <div class="px-5 flex-1">
            <ul class="flex justify-end text-lg">
                <li>
                    <a href="#" class="p-3 text-orange-500 hover:text-orange-400 font-black">Book PRO</a>
                </li>
                <li>
                    <a href="#" class="p-3 ml-2 font-medium text-gray-600">Pricing</a>
                </li>
                <li>
                    <a href="#" class="p-3 ml-2 font-medium text-gray-600">FAQs</a>
                </li>
                <li>
                    <a href="/member/login" class="p-3 ml-2 font-medium text-gray-600">Sign In</a>
                </li>
                <li>
                    <a href="/member/register" class="py-3 px-6 rounded hover:bg-orange-500 hover:text-white hover:border-orange-700 ml-2 border border-gray-600 font-medium text-gray-600">Join</a>
                </li>
            </ul>
        </div>
    </div>
</header>