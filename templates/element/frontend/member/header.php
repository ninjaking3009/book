<header id="main-header" class="w-full">
    <div class="w-full bg-white  px-16 py-5 flex items-center border-b border-gray-400">
        <div class="items-center">
            <a href="/">
                <img class="h-16" src="<?= $this->Url->image('logo.png') ?>" alt="">
            </a>
        </div>
        <div class="ml-10 w-1/4 items-center">
            <?= $this->Form->create(); ?>
            <div class="flex">
                <input type="text" class="w-full font-thin text-sm rounded-l border px-3 py-2 focus:outline-none" placeholder="Search Book">
                <input type="button" class="font-thin  text-sm hover:bg-orange px-3 py-2 rounded-r text-white focus:outline-none bg-blue-800" value="Search">
            </div>
            <?= $this->Form->end(); ?>
        </div>
        <?php
        if ($this->Identity->get()) :
        ?>
            <div class="px-5 flex-1">
                <div class="flex relative inline-block float-right">

                    <div x-data="{ isOpen: false }" class="relative inline-block text-left">
                        <div>
                            <span class="rounded-md shadow-sm">
                                <button type="button" @click="isOpen = !isOpen" class="flex items-center focus:outline-none mr-3" aria-haspopup="true" aria-expanded="true">
                                    <!-- <img class="w-8 h-8 rounded-full mr-4" src="http://i.pravatar.cc/300" alt="Avatar of User"> --> <span class="hidden md:inline-block font-light capitalize text-gray-600 text-sm"><i class="fal fa-user pr-3"></i> Hi, <?= $this->Identity->get('first_name') ?> </span>
                                    <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </button>
                            </span>
                        </div>
                        <div x-show="isOpen" x-transition:enter="transition ease-out duration-100 transform" x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100" x-transition:leave="transition ease-in duration-75 transform" x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95" class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg">
                            <div class="rounded-md bg-white shadow-xs">
                                <div class="py-4 px-3" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                                    <ul class="list-reset">
                                        <li>
                                            <a href="/member/profile" class="flex items-start px-4 py-2 text-gray-900 hover:text-orange-600 no-underline hover:no-underline">
                                                Profile
                                                <i class="ml-auto mt-0.5 pl-4 fal fa-user"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/member/notification" class="flex items-start px-4 py-2 text-gray-900 hover:text-orange-600 no-underline hover:no-underline">
                                                Notification
                                                <i class="ml-auto mt-0.5 pl-4 fal fa-bell"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <hr class="border-t my-3 border-gray-400">
                                        </li>
                                        <li>
                                            <a href="/member/logout" class="flex items-start px-4 py-2 text-gray-900 hover:text-orange-600 no-underline hover:no-underline">
                                                Logout
                                                <i class="ml-auto mt-0.5 pl-4 fal fa-sign-out"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php
        endif;
        ?>
    </div>
</header>