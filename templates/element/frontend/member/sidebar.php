<div class="bg-white w-60 pb-5 mr-10 border border-cool-gray-300 rounded-md">
            
            <h5 class="text-cool-gray-400 pt-5 px-5 font-semibold mb-3 text-sm">Welcome! <?= ucfirst($this->Identity->get('first_name')) ?></h5>
            <ul class="divide-y divide-gray-200 text-sm">
                <li><a href="/member/" class="flex px-5 border-l-4 border-orange-400 py-3 text-cool-gray-500 items-center">
                    <i class="fal fa-home mr-5"></i>
                    Home
                </a></li>
                <li><a href="/member/books/owned" class="border-l-4 px-5 border-gray-400 py-3 flex text-cool-gray-500 items-center">
                    <i class="fal fa-book mr-5"></i>
                    Your Books
                </a></li>
                <li><a href="#" class="border-l-4 px-5 border-gray-400 py-3 flex text-cool-gray-500 items-center">
                    <i class="fal fa-bell mr-5"></i>
                    Notifications
                </a></li>
                <li><a href="#" class="border-l-4 px-5 border-gray-400 py-3 flex text-cool-gray-500 items-center">
                    <i class="fal fa-user mr-5"></i>
                    Profile
                </a></li>
            </ul>

            <h5 class="text-cool-gray-400 pt-5 px-5 font-semibold mb-3 text-sm">My Books</h5>
            <div class="text-sm text-cool-gray-300 text-center mb-3">You dont have any books.</div>
            <a href="/member/books/add" class="text-orange-400 text-sm text-center block">Click here to add <i class="fal fa-plus-circle"></i></a>

            <h5 class="text-cool-gray-400 pt-5 px-5 font-semibold mb-3 text-sm">Books to Review</h5>
            <div class="text-sm text-cool-gray-300 text-center mb-3">You have no pending books to review.</div>
            <a href="/member/books/index" class="text-orange-400 text-sm text-center block">Click here to browse <i class="fal fa-search-plus"></i></a>

        </div>