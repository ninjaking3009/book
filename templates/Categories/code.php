<div class="container mx-auto py-10">
    <div class="mb-5">
        <ul class="flex font-light text-sm items-center">
            <li>
                <a href="#" class="text-gray-600">Fhire</a>
            </li>
            <li class="mx-2 text-xs text-gray-400"><i class="fa fa-chevron-right"></i></li>
            <li>
                <a href="#" class="text-gray-600">Programming & Tech</a>
            </li>
            <li class="mx-2 text-xs text-gray-400"><i class="fa fa-chevron-right"></i></li>
        </ul>
    </div>
    <h1 class="text-3xl font-bold text-gray-800 ">Wordpress</h1>
    <div class="font-light text-gray-600 ">
        Want to say more with less? Brand recognition is just a custom logo design away
    </div>
    <div class="flex items-center mt-10 mb-8 border-t border-gray-300 pt-5 ">
        <div class="flex-1">
            <div x-data="{ isOpen: false }" class="relative inline-block text-left">
                <div>
                    <span class="rounded-md shadow-sm">
                        <button type="button" @click="isOpen = !isOpen"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
                            id="options-menu" aria-haspopup="true" aria-expanded="true">
                            Service Options
                            <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd" />
                            </svg>
                        </button>
                    </span>
                </div>
                <div x-show="isOpen" x-transition:enter="transition ease-out duration-100 transform"
                    x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100"
                    x-transition:leave="transition ease-in duration-75 transform"
                    x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95"
                    class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg">
                    <div class="rounded-md bg-white shadow-xs">
                        <div class="py-4 px-5" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                            <div class="font-semibold text-gray-500 mb-2">Specialization</div>
                            <div class="grid grid-cols-2">
                                <div>Blog</div>
                                <div>Blog</div>
                                <div>Blog</div>
                                <div>Blog</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div x-data="{ isOpen: false }" class="relative inline-block text-left">
                <div>
                    <span class="rounded-md shadow-sm">
                        <button type="button" @click="isOpen = !isOpen"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
                            id="options-menu" aria-haspopup="true" aria-expanded="true">
                            Seller Details
                            <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd" />
                            </svg>
                        </button>
                    </span>
                </div>
                <div x-show="isOpen" x-transition:enter="transition ease-out duration-100 transform"
                    x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100"
                    x-transition:leave="transition ease-in duration-75 transform"
                    x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95"
                    class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg">
                    <div class="rounded-md bg-white shadow-xs">
                        <div class="py-4 px-5" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                            <div class="font-semibold text-gray-500 mb-2">Specialization</div>
                            <div class="grid grid-cols-2">
                                <div>Blog</div>
                                <div>Blog</div>
                                <div>Blog</div>
                                <div>Blog</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div x-data="{ isOpen: false }" class="relative inline-block text-left">
                <div>
                    <span class="rounded-md shadow-sm">
                        <button type="button" @click="isOpen = !isOpen"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
                            id="options-menu" aria-haspopup="true" aria-expanded="true">
                            Budget
                            <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd" />
                            </svg>
                        </button>
                    </span>
                </div>
                <div x-show="isOpen" x-transition:enter="transition ease-out duration-100 transform"
                    x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100"
                    x-transition:leave="transition ease-in duration-75 transform"
                    x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95"
                    class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg">
                    <div class="rounded-md bg-white shadow-xs">
                        <div class="py-4 px-5" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                            <div class="font-semibold text-gray-500 mb-2">Specialization</div>
                            <div class="grid grid-cols-2">
                                <div>Blog</div>
                                <div>Blog</div>
                                <div>Blog</div>
                                <div>Blog</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="flex">
                <div class=" text-gray-500 px-3">Pro Services</div>
                <div class=" text-gray-500 px-3">Local Sellers</div>
                <div class="text-gray-500 px-3">Online Sellers</div>
            </div>
        </div>
    </div>
    <div class="flex my-10 items-center">
        <div class="flex-1 pl-2 items-center">
            <div class="text-gray-500 text-sm">2,015 Services available</div>
        </div>
        <div class="text-gray-400">
            Sort By <div x-data="{ isOpen: false }" class="relative inline-block text-left">
                <div>
                    <span class="rounded-md shadow-sm">
                        <button type="button" @click="isOpen = !isOpen"
                            class="inline-flex ml-2 bg-white font-bold text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
                            id="options-menu" aria-haspopup="true" aria-expanded="true">
                            Best Selling
                            <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd" />
                            </svg>
                        </button>
                    </span>
                </div>
                <div x-show="isOpen" x-transition:enter="transition ease-out duration-100 transform"
                    x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100"
                    x-transition:leave="transition ease-in duration-75 transform"
                    x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95"
                    class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg">
                    <div class="rounded-md bg-white shadow-xs">
                        <div class="py-4 px-5" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                            <div>Best Selling</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid grid-cols-4 gap-10">
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
        <div class="border border-gray-300 shadow-md">
            <div>
                <img src="https://picsum.photos/350/200" alt="">
            </div>
            <div class="bg-white px-5 py-3 flex items-center">
                <div class="rounded-full overflow-hidden">
                    <img src="https://picsum.photos/40/40" alt="">
                </div>
                <div class="ml-5">
                    <div class="font-light text-gray-600">bryancoder</div>
                    <div class="font-light text-sm text-gray-400">Level 1 Seller</div>
                </div>
            </div>
            <div class="mb-4 px-5 text-gray-600">
                I will create stunning wordpress landing page
            </div>
            <div class="flex">
                <div class="flex-1  text-sm text-yellow-300 px-5 pb-5">
                    <i class="fa fa-star"></i>
                    5.0 <span class="text-gray-300">(300)</span>
                </div>
            </div>
        </div>
    </div>

</div>
