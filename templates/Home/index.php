<div class="bg-cover py-44 bg-cool-gray-400 w-full" style="background-image:url('https://picsum.photos/1800/500')"></div>

<div class="container mx-auto">
    <div class="py-10">
        <h2 class="text-xl font-bold text-gray-700 mb-10">Popular Books Samples</h2>
        <div class="slick-slider">
            <div class="px-5 mx-5 py-10 rounded bg-cover bg-center" style="background-image: url('https://picsum.photos/200/400?grayscale&blur=2')">
                <div class="text-white">J.K Rowling</div>
                <div class="text-white text-xl font-bold mb-20">Lethal White</div>
            </div>
            <div class="px-5 mx-5 py-10 rounded bg-cover bg-center" style="background-image: url('https://picsum.photos/200/400?grayscale&blur=2')">
                <div class="text-white">J.K Rowling</div>
                <div class="text-white text-xl font-bold mb-20">Lethal White</div>
            </div>
            <div class="px-5 mx-5 py-10 rounded bg-cover bg-center" style="background-image: url('https://picsum.photos/200/400?grayscale&blur=2')">
                <div class="text-white">J.K Rowling</div>
                <div class="text-white text-xl font-bold mb-20">Lethal White</div>
            </div>
            <div class="px-5 mx-5 py-10 rounded bg-cover bg-center" style="background-image: url('https://picsum.photos/200/400?grayscale&blur=2')">
                <div class="text-white">J.K Rowling</div>
                <div class="text-white text-xl font-bold mb-20">Lethal White</div>
            </div>
            <div class="px-5 mx-5 py-10 rounded bg-cover bg-center" style="background-image: url('https://picsum.photos/200/400?grayscale&blur=2')">
                <div class="text-white">J.K Rowling</div>
                <div class="text-white text-xl font-bold mb-20">Lethal White</div>
            </div>
            <div class="px-5 mx-5 py-10 rounded bg-cover bg-center" style="background-image: url('https://picsum.photos/200/400?grayscale&blur=2')">
                <div class="text-white">J.K Rowling</div>
                <div class="text-white text-xl font-bold mb-20">Lethal White</div>
            </div>
        </div>
    </div>

    <div class="flex mt-32 mb-32">
        <div class="flex-1">
            <img src="https://picsum.photos/600/400" alt="">
        </div>
        <div class="flex-1 pl-20">
            <h2 class="text-xl font-bold text-gray-700 mb-10">Unlimited Reviews by Real Readers</h2>
            <div class="font-bold"><i class="fa fa-check text-green-500"></i> Payment protection, guaranteed</div>
            <div class="mb-10">Support other authors to earn snaps and get reviewed.</div>
            <div class="font-bold"><i class="fa fa-check text-green-500"></i> Payment protection, guaranteed</div>
            <div class="mb-10">Support other authors to earn snaps and get reviewed.</div>
            <div class="font-bold"><i class="fa fa-check text-green-500"></i> Payment protection, guaranteed</div>
            <div class="mb-10">Support other authors to earn snaps and get reviewed.</div>
        </div>
    </div>

</div>

<div class="bg-cool-gray-100 py-20 mb-24">
    <div class="container mx-auto">
        <div class="">
            <h2 class="text-xl text-center font-bold text-gray-700 mb-16">What We Offer</h2>
            <div class="grid grid-cols-4 gap-x-4 gap-y-16">
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
                <div class="text-center">
                    <div class="text-yellow-300">
                        <i class="fa fa-star text-3xl"></i>
                    </div>
                    <div class="font-light mt-5 text-gray-700">Generate Reviews</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mx-auto">
    <div class="pb-10 pt-5">
        <h2 class="text-xl font-bold text-gray-700 mb-10">Get inspired with reviews made by our users</h2>
        <div class="slick-slider pb-10">
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>
            <div class="rounded shadow-md mx-4">
                <img src="https://picsum.photos/350/300" alt="">
                <div class="bg-white px-5 py-3 flex items-center">
                    <div class="rounded-full overflow-hidden">
                        <img src="https://picsum.photos/40/40" alt="">
                    </div>
                    <div class="ml-5">
                        <div class="font-bold text-gray-600">Romeo and Juliet</div>
                        <div class="font-light text-sm text-gray-500">by William Shakespeare</div>
                    </div>
                </div>
            </div>

        </div>
    </div>


</div>