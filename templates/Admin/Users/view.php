<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="px-10 py-8 w-full">
    <div class="flex justify-between align-center">
        <h2 class="text-2xl font- text-cool-gray-600 leading-tight"><?= __('User') ?></h2>
        <div class="flex-inline mr-2">
            <?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id], ['class' => 'bg-transparent hover:bg-orange-500 text-orange-900 text-sm font-semibold hover:text-white py-2 px-4 border border-orange-500 hover:border-transparent rounded']) ?>
            <?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'bg-transparent hover:bg-red-500 text-red-900 text-sm font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded']) ?>
            <?= $this->Html->link(__('List Users'), ['action' => 'lists'], ['class' => 'bg-transparent hover:bg-blue-500 text-blue-900 text-sm font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded']) ?>
        </div>
    </div>

    <div class="container mx-auto mt-10">
        <table>
            <tr>
                <th><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>
            <tr>
                <th><?= __('First Name') ?></th>
                <td><?= h($user->first_name) ?></td>
            </tr>
            <tr>
                <th><?= __('Last Name') ?></th>
                <td><?= h($user->last_name) ?></td>
            </tr>
            <tr>
                <th><?= __('Status') ?></th>
                <td><?= h($user->status) ?></td>
            </tr>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($user->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Date Created') ?></th>
                <td><?= h($user->date_created) ?></td>
            </tr>
            <tr>
                <th><?= __('Is Admin') ?></th>
                <td><?= $user->is_admin ? __('Active') : __('Inactive'); ?></td>
            </tr>
        </table>
    </div>
</div>