<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="users px-10">
    <div class="py-8">

        <div class="flex justify-between align-middle">
            <h2 class="text-2xl font- text-cool-gray-600 leading-tight"><?= __('Users') ?></h2>

            <div class="flex-inline mr-2">
            </div>
        </div>

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div class="inline-block min-w-full border border-cool-gray-200 overflow-hidden">
                <table class="min-w-full leading-normal">
                    <thead>
                        <tr>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('id') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('email') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('first_name') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('last_name') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('status') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= $this->Paginator->sort('date_created') ?>
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-blue-50 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                <?= __('Actions') ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user) : ?>
                            <tr>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= $this->Number->format($user->id) ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= h($user->email)  ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= h($user->first_name)  ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= h($user->last_name)  ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= $user->status ? "Active" : "Inactive"   ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm"><?= h($user->date_created)  ?></td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm flex-inline">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['class' => 'px-2 py-1 mx-1 border border-green-600 rounded-sm hover:bg-green-500 hover:text-white text-xs']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id], ['class' => 'px-2 py-1 mx-1 border border-orange-600 rounded-sm hover:bg-orange-500 hover:text-white text-xs']) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['class' => 'px-2 py-1 mx-1 border border-red-600 rounded-sm hover:bg-red-500 hover:text-white text-xs'], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="px-5 py-2 bg-white border-t flex items-center justify-between">
                    <span class="text-xs xs:text-sm text-gray-500">
                        <?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?>
                    </span>
                    <div class="inline-flex">
                        <?= $this->Paginator->prev('Previous') ?>
                        <?= $this->Paginator->next('Next') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>