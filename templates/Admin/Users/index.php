<?php
ob_start();
?>
<style>
    #calc-form-wrapper {
        display:flex;
    }
    #calc-form-wrapper > div {
        width: 33.33%;
        margin: 0 10px;
    }
    #calc-form-wrapper > div > label {
        display:block;
        margin-bottom: 10px;
    }
    #calc-form-wrapper > div > input[type="number"] {
        display:block;
        width: 100%;
        border: 1px solid #ccc;
        padding: 5px 15px;
    }
    #calc-table-wrapper {
        width: 100%;
        margin-top: 20px;
    }
    #calc-table-wrapper table {
        width: 100%;
        border: 1px solid #ccc;
    }
    #calc-table-wrapper table td {
        width: 33.33%;
        padding: 10px;
        background: #f8f8f8;
        border: 1px solid #ccc;
    }
</style>


<div id="calc-wrapper">
    <div id="calc-form-wrapper">
        <div>
            <label for="">Property Price</label>
            <input type="number" onkeyup="compute()" value="0" id="property-price">
        </div>
        <div>
            <label for="">Deposit</label>
            <input type="number" onkeyup="compute()" value="0" id="deposit">
        </div>
        <div>
            <label for="">Term Years</label>
            <input type="number" onkeyup="compute()" value="0" id="term">
        </div>
    </div>
    <div id="calc-table-wrapper">
        <table>
            <tr>
                <td>Deposit</td>
                <td id="deposit-percent">0%</td>
                <td id="deposit-amount">£0.00</td>
            </tr>
            <tr>
                <td>Equity Loan</td>
                <td>20%</td>
                <td id="equity-amount">£0.00</td>
            </tr>
            <tr>
                <td>Mortgage</td>
                <td id="mortgage-percent">0%</td>
                <td id="mortgage-amount">£0.00</td>
            </tr>
            <tr>
                <td>Monthly Payment</td>
                <td id="monthly-payment" colspan="2">£0.00</td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
    let propertPrice = 0;
    let deposit = 0;
    let term = 0;
    const fixedRate = 0.2;
    let months = 0;
    let equityLoan = 0;
    let mortgage = 0;
    let monthlyPayment = 0;
    let depositPercent = 0;
    let mortgagePercent = 0;

    function update() {
        propertPrice = $('#property-price').val() ?? 0;
        deposit = $('#deposit').val() ?? 0;
        term = $('#term').val() ?? 0;
    }

    function compute() {
        update();

        if(propertPrice == 0 || deposit == 0 || term == 0) return false;

        console.log(propertPrice,deposit,term);

        months = term * 12;
        depositPercent = (deposit / propertPrice) * 100;
        equityLoan = propertPrice * fixedRate;
        mortgage = propertPrice - equityLoan - deposit;
        mortgagePercent = Math.round(((80 - depositPercent)).toFixed(2));
        monthlyPayment = PMT((0.02/12),months,mortgage);

        updateFields();
    }

    function updateFields() {
        $('#deposit-percent').text(String(depositPercent) + "%");
        $('#mortgage-percent').text(String(mortgagePercent) + "%");
        $('#deposit-amount').text(formatMoney(deposit));
        $('#equity-amount').text(formatMoney(equityLoan));
        $('#mortgage-amount').text(formatMoney(mortgage));
        $('#monthly-payment').text(formatMoney(monthlyPayment));
    }

    function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? "-" : "";

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            let total = negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            return "£" + String(total);
        } catch (e) {
            console.log(e)
        }
    };

    function PMT(ir, np, pv, fv, type) {
        /*
        * ir   - interest rate per month
        * np   - number of periods (months)
        * pv   - present value
        * fv   - future value
        * type - when the payments are due:
        *        0: end of the period, e.g. end of month (default)
        *        1: beginning of period
        */
        var pmt, pvif;

        fv || (fv = 0);
        type || (type = 0);

        if (ir === 0)
            return -(pv + fv)/np;

        pvif = Math.pow(1 + ir, np);
        pmt = - ir * pv * (pvif + fv) / (pvif - 1);

        if (type === 1)
            pmt /= (1 + ir);

        return pmt;
    }
</script>