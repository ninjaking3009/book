<div class="bg-cool-gray-100 min-h-screen flex flex-col">
    <div class="container max-w-md mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <h1 class="mb-8 text-3xl text-center">Sign in</h1>

            <?= $this->Flash->render() ?>
            <?php echo $this->Form->create(); ?>

            <?php echo $this->Form->input('email', ['class' => 'block border border-grey-light w-full p-3 rounded mb-4', 'placeholder' => 'Email']); ?>

            <?php echo $this->Form->password('password', ['class' => 'block border border-grey-light w-full p-3 rounded mb-4', 'placeholder' => 'Password']); ?>

            <button type="submit" class="w-full text-center py-3 rounded bg-blue-800 text-white hover:bg-green-dark focus:outline-none my-1">Login</button>
            <?php echo $this->Form->end(); ?>

        </div>
    </div>
</div>