<?php

/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $book
 */
?>
<div class="px-10 py-8 w-full">

    <div class="flex justify-between align-center mb-8">
        <h2 class="text-2xl font- text-cool-gray-400 leading-tight"><?= __('Book ID #' . $book->id) ?></h2>
        <div class="flex-inline mr-2">
            <?= $this->Html->link(__('Edit Book'), ['action' => 'edit', $book->id], ['class' => 'bg-transparent hover:bg-orange-500 text-orange-900 text-sm font-semibold hover:text-white py-2 px-4 border border-orange-500 hover:border-transparent rounded']) ?>
            <?= $this->Form->postLink(__('Delete Book'), ['action' => 'delete', $book->id], ['confirm' => __('Are you sure you want to delete # {0}?', $book->id), 'class' => 'bg-transparent hover:bg-red-500 text-red-900 text-sm font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded']) ?>
            <?= $this->Html->link(__('List Books'), ['action' => 'index'], ['class' => 'bg-transparent hover:bg-blue-500 text-blue-900 text-sm font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded']) ?>
            <?= $this->Html->link(__('New Book'), ['action' => 'add'], ['class' => 'bg-transparent hover:bg-green-500 text-green-900 text-sm font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded']) ?>
        </div>
    </div>

    <div class="flex">
        <div class="w-1/4">
            <img class="w-full" src="<?= DS . 'book_covers' . DS . $book->cover ?>" onerror="this.src='<?= DS . 'book_covers' . DS . 'default_book_cover' ?>'" alt="">
        </div>
        <div class="flex-1 pl-10 pt-5">
            <h1 class="text-xl font-semibold"><?= h($book->title) ?></h1>
            <h2 class="text-cool-gray-700"><?= h($book->author) ?></h2>
            <a href="<?= h($book->amazon_url) ?>" class="inline-block text-white rounded-sm px-3 py-1 mt-3 text-sm border border-blue-700 bg-blue-500" target="_blank">Amazon Link</a>
            <div class="mt-5 border-t pt-5">
                <div class="mb-3">
                    <span class="block text-sm text-cool-gray-500 font-light">Category</span>
                    <div class="inline-flex">
                        <?php foreach ($book->book_categories as $category) : ?>
                            <a href="#" class="mr-2 underline text-orange-400"> <?= $category->category->name ?> </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="mb-3">
                    <span class="block text-sm text-cool-gray-500 font-light">Tags</span>
                    <div class="inline-flex">
                        <?php foreach ($book->book_tags as $tag) : ?>
                            <a href="#" class="mr-2 underline text-orange-400"> <?= $tag->name ?> </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="flex mb-3">
                    <div>
                        <span class="block text-sm text-cool-gray-500 font-light">Publisher</span>
                        <?= h($book->user->full_name) ?>
                    </div>
                    <div class="ml-10">
                        <span class="block text-sm text-cool-gray-500 font-light">Status</span>
                        <?= h($book->status ? 'Active' : 'Inactive') ?>
                    </div>
                    <div class="ml-10">
                        <span class="block text-sm text-cool-gray-500 font-light">Date Added</span>
                        <?= h($book->date_added) ?>
                    </div>
                    <div class="ml-10">
                        <span class="block text-sm text-cool-gray-500 font-light">Date Updated</span>
                        <?= h($book->date_updated) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h2 class="text-cool-gray-400 border-b border-cool-gray-300 pb-3 mb-5 mt-5">Additional Information</h2>
    <div class="grid grid-flow-row w-full grid-cols-2 grid-row-2 gap-5">
        <?php foreach ($book->book_question_answer as $k => $v) : ?>
            <div class="mb-5">
                <div class="text-cool-gray-400 mb-1"><?= $v->question->question ?></div>
                <?php if ($v->question->type == 'selection') : ?>
                    <ul class="list-disc ml-10">
                        <?php
                        $answers = explode('|', $v->answer);
                        foreach ($answers as $x => $y) :
                        ?>
                            <li><?= $y ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php else : ?>
                    <div><?= $v->answer ?></div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>