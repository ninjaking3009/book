<?php

/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $book
 */
?>

<div class="books form px-10">
    <div class="py-8 max-w-2xl mx-auto">

        <div class="flex justify-between align-middle">
            <h2 class="text-2xl font- text-cool-gray-600 leading-tight"><?= __('Edit Book') ?></h2>
            <div class="flex-inline mr-2">
                <?= $this->Form->postLink(
    __('Delete'),
    ['action' => 'delete', $book->id],
    ['confirm' => __('Are you sure you want to delete this record?'), 'class' => 'bg-transparent hover:bg-red-500 text-red-900 text-sm font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded']
) ?>
                <?= $this->Html->link(__('List Books'), ['action' => 'index'], ['class' => 'bg-transparent hover:bg-green-500 text-green-900 text-sm font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded']) ?>
            </div>
        </div>

        <div class="mt-8 pt-8 border-t border-gray-400 overflow-x-auto">
            <?= $this->Form->create($book, ['enctype' => 'multipart/form-data']) ?>
            <div class='flex flex-wrap mb-6'>
                <?php
                echo $this->Form->control('user_id', ['options' => $users]);
                echo $this->Form->control('title');
                echo $this->Form->control('author');
                echo $this->Form->control('amazon_url');
                echo $this->Form->control('book_categories', ['options' => $categories,'multiple' => true, 'label' => 'Book Categories (Select Multiple)']);
                echo $this->Form->control('book_tags', ['label' => 'Tags (Separated by comma)','placeholder' => 'Enter Tags']);

                echo $this->Form->control('coverTmp', [
                    'type' => 'file',
                    'label' => 'Cover Image'
                ]);
                echo $this->Form->control('status');
                ?>
                <h3 class="border-b border-cool-gray-300 block w-full pb-3 mb-5 mt-5 text-cool-gray-500">Additional Information</h3>
                <?php foreach ($questions as $k => $v): ?>
                    <?php if ($v->type == 'text'): ?>
                        <div class="relative w-full appearance-none label-floating mb-3 textarea required">
                            <textarea class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500" name="question[<?= $v->id ?>][answer]" required="required" id="question" rows="5"><?= ($v->book_question_answer[0]) ? $v->book_question_answer[0]->answer : "" ?></textarea>
                            <label class="absolute tracking-wide py-2 px-4 mb-4 hidden leading-tight top-0 left-0 cursor-text"><?= $v->question ?></label>
                        </div>
                    <?php else: ?>
                        <div class="mb-3"><?= ucfirst($v->question) ?></div>
                        <div class="grid grid-flow-row w-full grid-cols-2 mb-5 grid-row-2 gap-4">
                        <?php foreach ($v->question_choices as $x => $y): ?>
                            <div>
                                <div class="relative w-full appearance-none label-floating mb-3 checkbox"><label for="questionChoice-<?= $k.'-'.$x ?>" class="inline-flex items-center"><input type="checkbox" name="question[<?= $v->id ?>][answer][]" class="form-checkbox" <?= (strpos($v->book_question_answer[0]->answer, $y->name) !== false) ? "checked" : "" ?> value="<?= $y->name ?>" id="questionChoice-<?= $k.'-'.$x ?>"> <span class="ml-2"><?= $y->name ?></span></label></div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                        
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <input type="submit" value="Submit" class="bg-transparent cursor-pointer float-right hover:bg-blue-500 text-blue-900 font-semibold hover:text-white py-2 px-6 border border-blue-500 hover:border-transparent rounded">
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>