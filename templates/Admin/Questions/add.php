<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $question
 */
?>

<div class="questions form px-10">
    <div class="py-8 max-w-2xl mx-auto">

        <div class="flex justify-between align-middle">
            <h2 class="text-2xl font- text-cool-gray-600 leading-tight"><?= __('Add Question') ?></h2>
            <div class="flex-inline mr-2">
                <?= $this->Html->link(__('List Questions'), ['action' => 'index'], ['class' => 'bg-transparent hover:bg-green-500 text-green-900 text-sm font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded']) ?>
            </div>
        </div>

        <div class="mt-8 pt-8 border-t border-gray-400 overflow-x-auto">
            <?= $this->Form->create($question) ?>
                <div class='flex flex-wrap mb-6'>
                    <?php
                        echo $this->Form->control('question');
                        echo $this->Form->control('status');
                        echo $this->Form->control('type', ['options' => ['text' => 'Text','selection' => 'Selection'],'label' => 'Question Type', 'id' => 'type']);
                    ?>
                    <div class="w-full hidden" id="selection-main-wrapper">
                        <div id="selection-wrapper" class="w-full border border-cool-gray-200 px-5 pt-5 pb-2 mb-5 rounded-sm"></div>
                        <a href="javascript:;" id="add-selection" class="inline-block px-3 py-2 text-sm bg-blue-500 text-white">Add Selection <i class="fal fa-plus"></i></a>
                    </div>
                </div>
                <input type="submit" value="Submit" class="bg-transparent cursor-pointer float-right hover:bg-blue-500 text-blue-900 font-semibold hover:text-white py-2 px-6 border border-blue-500 hover:border-transparent rounded">
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>

    document.getElementById('type').addEventListener('change', function () {
        const mainWrapper = document.getElementById('selection-main-wrapper');
        console.log(this.value);
        if(this.value == 'selection') {
            mainWrapper.classList.remove('hidden');
        } else {
            mainWrapper.classList.add('hidden');
        }
    });

    addField();

    const btn = document.getElementById('add-selection');
    btn.addEventListener('click',function(){
        addField();
    });

    function addField() {
        const selectionWrapper = document.getElementById('selection-wrapper');
        const selection = document.createElement('input')
        const wrapper = document.createElement('div');
        const removeBtn = document.createElement('i');

        wrapper.setAttribute('class','w-full mb-3 flex items-center');
        selection.setAttribute('class','tracking-wide py-2 px-4 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500 mr-5');
        selection.setAttribute('placeholder', 'Add Selection');
        selection.setAttribute('name','selections[]');
        removeBtn.setAttribute('class','fal fa-trash-alt remove-btn-selection cursor-pointer fal fa-trash-alt remove-btn-selection text-white bg-red-500 p-2 rounded-sm');

        removeBtn.addEventListener('click',function(e){
            this.parentNode.remove();
        });

        wrapper.appendChild(selection);
        wrapper.appendChild(removeBtn);
        selectionWrapper.appendChild(wrapper);
    }
</script>