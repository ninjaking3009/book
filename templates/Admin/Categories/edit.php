<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $category
 */
?>

<div class="categories form px-10">
    <div class="py-8 max-w-2xl mx-auto">

        <div class="flex justify-between align-middle">
            <h2 class="text-2xl font- text-cool-gray-600 leading-tight"><?= __('Edit Category') ?></h2>
            <div class="flex-inline mr-2">
                <?= $this->Form->postLink(
    __('Delete'),
    ['action' => 'delete', $category->id],
    ['confirm' => __('Are you sure you want to delete this record?'), 'class' => 'bg-transparent hover:bg-red-500 text-red-900 text-sm font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded']
) ?>
    <?= $this->Html->link(__('List Categories'), ['action' => 'index'], ['class' => 'bg-transparent hover:bg-green-500 text-green-900 text-sm font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded']) ?>
            </div>
        </div>

        <div class="mt-8 pt-8 border-t border-gray-400 overflow-x-auto">
            <?= $this->Form->create($category) ?>
                <div class='flex flex-wrap mb-6'>
                    <?php
                        echo $this->Form->control('name');
                        echo $this->Form->control('status');
                    ?>
                </div>
                <input type="submit" value="Submit" class="bg-transparent cursor-pointer float-right hover:bg-blue-500 text-blue-900 font-semibold hover:text-white py-2 px-6 border border-blue-500 hover:border-transparent rounded">
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>