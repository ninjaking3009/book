<?php

/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $category
 */
?>
<div class="px-10 py-8 w-full">
    <div class="flex justify-between align-center">
        <h2 class="text-2xl font- text-cool-gray-600 leading-tight"><?= __('Add Category') ?></h2>
        <div class="flex-inline mr-2">
            <?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id], ['class' => 'bg-transparent hover:bg-orange-500 text-orange-900 text-sm font-semibold hover:text-white py-2 px-4 border border-orange-500 hover:border-transparent rounded']) ?>
            <?= $this->Form->postLink(__('Delete Category'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'class' => 'bg-transparent hover:bg-red-500 text-red-900 text-sm font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded']) ?>
            <?= $this->Html->link(__('List Categories'), ['action' => 'index'], ['class' => 'bg-transparent hover:bg-blue-500 text-blue-900 text-sm font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded']) ?>
            <?= $this->Html->link(__('New Category'), ['action' => 'add'], ['class' => 'bg-transparent hover:bg-green-500 text-green-900 text-sm font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded']) ?>
        </div>
    </div>

    <div class="container mx-auto mt-10">
        <table>
            <tr>
                <th><?= __('Name') ?></th>
                <td><?= h($category->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($category->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Status') ?></th>
                <td><?= $category->status ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
    </div>
</div>