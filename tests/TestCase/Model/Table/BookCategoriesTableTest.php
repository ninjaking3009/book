<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookCategoriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookCategoriesTable Test Case
 */
class BookCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BookCategoriesTable
     */
    protected $BookCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.BookCategories',
        'app.Books',
        'app.Categories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('BookCategories') ? [] : ['className' => BookCategoriesTable::class];
        $this->BookCategories = $this->getTableLocator()->get('BookCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->BookCategories);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
