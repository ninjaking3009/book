<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuestionChoicesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuestionChoicesTable Test Case
 */
class QuestionChoicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuestionChoicesTable
     */
    protected $QuestionChoices;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.QuestionChoices',
        'app.Questions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('QuestionChoices') ? [] : ['className' => QuestionChoicesTable::class];
        $this->QuestionChoices = $this->getTableLocator()->get('QuestionChoices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->QuestionChoices);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
