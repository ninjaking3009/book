<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookQuestionAnswerTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookQuestionAnswerTable Test Case
 */
class BookQuestionAnswerTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BookQuestionAnswerTable
     */
    protected $BookQuestionAnswer;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.BookQuestionAnswer',
        'app.Books',
        'app.Questions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('BookQuestionAnswer') ? [] : ['className' => BookQuestionAnswerTable::class];
        $this->BookQuestionAnswer = $this->getTableLocator()->get('BookQuestionAnswer', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->BookQuestionAnswer);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
