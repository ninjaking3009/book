<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookTagsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookTagsTable Test Case
 */
class BookTagsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BookTagsTable
     */
    protected $BookTags;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.BookTags',
        'app.Books',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('BookTags') ? [] : ['className' => BookTagsTable::class];
        $this->BookTags = $this->getTableLocator()->get('BookTags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->BookTags);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
