var areaChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    cubicInterpolationMode: 'monotone',
    legend: {
        display: true
    },
    tooltips: {
        mode: 'point'
    },
    scales: {
        xAxes: [{
            gridLines: {
                display: false,
            }
        }],
        yAxes: [{
            gridLines: {
                display: false,
            }
        }]
    }
}