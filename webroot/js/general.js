// Start Sticky Header
// When the user scrolls the page, execute myFunction
window.onscroll = function() {setStickyHeader()};

// Get the navbar
var navbar = document.getElementById("main-header");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function setStickyHeader() {
  if (window.pageYOffset >= sticky) {
    //navbar.classList.add("fixed")
  } else {
    //navbar.classList.remove("fixed");
  }
}
// End Sticky Header


//Home Slick Slider
$('.slick-slider').slick({
    lazyLoad: 'ondemand',
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 5,
});