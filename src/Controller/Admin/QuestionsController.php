<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Questions Controller
 *
 * @method \App\Model\Entity\Question[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuestionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $questions = $this->paginate($this->Questions);

        $this->set(compact('questions'));
    }

    /**
     * View method
     *
     * @param string|null $id Question id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $question = $this->Questions->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('question'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $question = $this->Questions->newEmptyEntity();
        if ($this->request->is('post')) {
            $raw = $this->request->getData();

            if ($raw['type'] == 'selection' && !empty($raw['selections'][0])) {
                $selections = [];
                foreach ($raw['selections'] as $key => $value) {
                    if (empty($value)) {
                        continue;
                    }
                    $selections[]['name'] = $value;
                }
                $raw['question_choices'] = $selections;
                unset($raw['selections']);
            }
            
            $question = $this->Questions->patchEntity($question, $raw, ['associated' => ['QuestionChoices']]);
            
            if ($this->Questions->save($question)) {
                $this->Flash->success(__('The question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The question could not be saved. Please, try again.'));
        }
        $this->set(compact('question'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Question id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $question = $this->Questions->get($id, [
            'contain' => ['QuestionChoices'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $raw = $this->request->getData();

            if ($raw['type'] == 'selection' && !empty($raw['selections'][0])) {
                $this->Questions->QuestionChoices->deleteAll(['question_id' => $id]);
                $selections = [];
                foreach ($raw['selections'] as $key => $value) {
                    if (empty($value)) {
                        continue;
                    }
                    $selections[$key]['name'] = $value;
                }
                $raw['question_choices'] = $selections;
                unset($raw['selections']);
            }

            $question = $this->Questions->patchEntity($question, $raw, ['associated' => ['QuestionChoices']]);
            
            if ($this->Questions->save($question)) {
                $this->Flash->success(__('The question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The question could not be saved. Please, try again.'));
        }
        $this->set(compact('question'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Question id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $question = $this->Questions->get($id);
        if ($this->Questions->delete($question)) {
            $this->Flash->success(__('The question has been deleted.'));
        } else {
            $this->Flash->error(__('The question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
