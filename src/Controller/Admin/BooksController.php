<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\ORM\Query;

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Books Controller
 *
 * @property \App\Model\Table\BooksTable $Books
 * @method \App\Model\Entity\Book[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BooksController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $books = $this->paginate($this->Books);
        $this->set(compact('books'));
    }

    /**
     * View method
     *
     * @param string|null $id Book id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $book = $this->Books->get($id, [
            'contain' => ['Users', 'BookCategories' => ['Categories'], 'BookQuestionAnswer' => ['Questions'], 'BookTags'],
        ]);

        $this->set(compact('book'));
    }

    public function update($id = null)
    {
        $book = $this->Books->get($id, [
            'contain' => ['Users', 'BookCategories' => ['Categories'], 'BookQuestionAnswer' => ['Questions'], 'BookTags'],
        ]);

        try {
            $this->loadComponent('Amazon', ['https://www.amazon.com/Fatal-Fairies-N-M-Brown-ebook/dp/B083S423M9']);
            $this->Amazon->getPrices();
        } catch (\Throwable $th) {
            $this->Flash->error(__('Unable to verify the URL. Please check the URL and try again.'));
        }
        exit();
        $this->set(compact('book'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $book = $this->Books->newEmptyEntity();
        if ($this->request->is('post')) {
            $raw = $this->request->getData();
            $raw['view_by_verified'] = 1;
            $raw['view_by_standard'] = 0;

            unset($raw['coverTmp']);

            $fileObject = $this->request->getData('coverTmp');

            $raw['cover'] = 'book_default_cover.jpg';

            if ($fileObject->getSize()) {
                $raw['cover'] = strtotime("now") . "_" . $fileObject->getClientFilename();
                $type = $fileObject->getClientMediaType();
                $size = $fileObject->getSize();
                $tmpName = $fileObject->getStream()->getMetadata('uri');
                $error = $fileObject->getError();
                $fileObject->moveTo(WWW_ROOT . 'book_covers' . DS . $raw['cover'], true);
            }

            if (trim($raw['book_tags']) != "") {
                $raw['book_tags'] = explode(',', $raw['book_tags']);
                $tmpTags = [];
                foreach ($raw['book_tags'] as $key => $value) {
                    $tmpTags[$key]['name'] = trim($value);
                }
                unset($key);
                unset($value);
                $raw['book_tags'] = $tmpTags;
            }

            $tmpCategory = [];
            foreach ($raw['book_categories'] as $key => $value) {
                $tmpCategory[$key]['category_id'] = $value;
            }
            $raw['book_categories'] = $tmpCategory;

            $book->setDirty('book_categories', true);
            $book->setDirty('book_tags', true);

            $raw['book_question_answer'] = [];

            foreach ($raw['question'] as $kq => $question) {
                $raw['book_question_answer'][$kq]['question_id'] = $kq;
                $raw['book_question_answer'][$kq]['answer'] = is_array($question['answer']) ? implode('|', $question['answer']) : $question['answer'];
            }

            $book = $this->Books->patchEntity($book, $raw, ['associated' => ['BookCategories', 'BookTags', 'BookQuestionAnswer']]);

            if ($this->Books->save($book)) {
                $this->Flash->success(__('The book has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The book could not be saved. Please, try again.'));
        }
        $users = $this->Books->Users->find('list', ['limit' => 200]);
        $categories = $this->Books->BookCategories->Categories->find('list');
        $this->loadModel('Questions');
        $questions = $this->Questions->find('all', ['contain' => ['QuestionChoices']])->toArray();

        $this->set(compact('book', 'users', 'categories', 'questions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Book id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $book = $this->Books->get($id, [
            'contain' => ['Users', 'BookCategories' => ['Categories'], 'BookQuestionAnswer' => ['Questions'], 'BookTags'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $raw = $this->request->getData();

            unset($raw['coverTmp']);

            $fileObject = $this->request->getData('coverTmp');

            if ($fileObject->getSize()) {
                $raw['cover'] = strtotime("now") . "_" . $fileObject->getClientFilename();
                $type = $fileObject->getClientMediaType();
                $size = $fileObject->getSize();
                $tmpName = $fileObject->getStream()->getMetadata('uri');
                $error = $fileObject->getError();
                $fileObject->moveTo(WWW_ROOT . 'book_covers' . DS . $raw['cover'], true);
            }

            if (trim($raw['book_tags']) != "") {
                $this->Books->BookTags->deleteAll(['book_id' => $id]);

                $raw['book_tags'] = explode(',', $raw['book_tags']);
                $tmpTags = [];
                foreach ($raw['book_tags'] as $key => $value) {
                    $tmpTags[$key]['name'] = trim($value);
                }
                unset($key);
                unset($value);
                $raw['book_tags'] = $tmpTags;
            }

            $tmpCategory = [];
            $this->Books->BookCategories->deleteAll(['book_id' => $id]);
            foreach ($raw['book_categories'] as $key => $value) {
                $tmpCategory[$key]['category_id'] = $value;
            }
            $raw['book_categories'] = $tmpCategory;

            $book->setDirty('book_categories', true);
            $book->setDirty('book_tags', true);

            $raw['book_question_answer'] = [];
            $this->Books->BookQuestionAnswer->deleteAll(['book_id' => $id]);
            foreach ($raw['question'] as $kq => $question) {
                $raw['book_question_answer'][$kq]['question_id'] = $kq;
                $raw['book_question_answer'][$kq]['answer'] = is_array($question['answer']) ? implode('|', $question['answer']) : $question['answer'];
            }


            $book = $this->Books->patchEntity($book, $raw, ['associated' => ['BookCategories', 'BookTags', 'BookQuestionAnswer']]);
            if ($this->Books->save($book)) {
                $this->Flash->success(__('The book has been saved.'));

                return $this->redirect(['action' => 'view', $id]);
            }
            $this->Flash->error(__('The book could not be saved. Please, try again.'));
        }

        $users = $this->Books->Users->find('list', ['limit' => 200]);
        $categories = $this->Books->BookCategories->Categories->find('list');
        $this->loadModel('Questions');
        $questions = $this->Questions->find('all', ['contain' => ['QuestionChoices', 'BookQuestionAnswer' => function (Query $q) use ($id) {
            return $q->where(['BookQuestionAnswer.book_id' => $id]);
        }]])->toArray();
        $book['book_tags'] = implode(',', array_map(function ($entry) {
            return $entry['name'];
        }, $book['book_tags']));

        $tmpCategory = array_map(function ($entry) {
            return $entry['category_id'];
        }, $book['book_categories']);

        $book['book_categories'] = $tmpCategory;

        $this->set(compact('book', 'users', 'categories', 'questions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Book id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $book = $this->Books->get($id);
        if ($this->Books->delete($book)) {
            $this->Flash->success(__('The book has been deleted.'));
        } else {
            $this->Flash->error(__('The book could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
