<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

class AmazonComponent extends Component
{
    public $url;
    public $client;
    public $crawler;
    public $prices;
    public $details;

    public function __construct($c, $url)
    {
        $this->url = $url[0];
        $this->client = new Client(HttpClient::create(['timeout' => 60]));
        return $this->getCrawler();
    }

    public function getCrawler()
    {
        try {
            $this->crawler = $this->client->request('GET', $this->url);
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    public function getPrices()
    {
        $this->crawler->filter('#tmmSwatches ul li')->each(function ($node) {
            $tmp['name'] = $node->filter('span.a-list-item span span a > span')->first()->text();
            $tmp['price'] = $node->filter('span.a-list-item span span a > span')->eq(1)->text();
            $this->prices[] = $tmp;
        });
        debug($this->prices);
        
        return $this->prices;
    }

    public function getDetails()
    {
        $this->crawler->filter('#detailBulletsWrapper_feature_div ul li')->each(function ($node) {
            $this->details[] = $node->text();
        });
        debug($this->crawler->filter('#editorialReviews_feature_div')->attr('class'));
        echo "hereee";
        debug($this->details);
    }
}
