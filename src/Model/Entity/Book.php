<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Book Entity
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property int $user_id
 * @property string $status
 * @property bool $view_by_standard
 * @property bool $view_by_verified
 * @property string $cover
 * @property string $amazon_url
 * @property \Cake\I18n\FrozenTime $date_added
 * @property \Cake\I18n\FrozenTime $date_updated
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\BookCategory[] $book_categories
 * @property \App\Model\Entity\BookQuestionAnswer[] $book_question_answer
 * @property \App\Model\Entity\BookTag[] $book_tags
 */
class Book extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'author' => true,
        'user_id' => true,
        'status' => true,
        'view_by_standard' => true,
        'view_by_verified' => true,
        'cover' => true,
        'amazon_url' => true,
        'date_added' => true,
        'date_updated' => true,
        'user' => true,
        'book_categories' => true,
        'book_question_answer' => true,
        'book_tags' => true,
    ];
}
