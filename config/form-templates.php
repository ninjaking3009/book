<?php

return [
    // Used for button elements in button().
    'button' => '<button{{attrs}}>{{text}}</button>',
    // Used for checkboxes in checkbox() and multiCheckbox().
    'checkbox' => '<input type="checkbox" name="{{name}}" class="form-checkbox" value="{{value}}"{{attrs}}>',
    // Input group wrapper for checkboxes created via control().
    'checkboxFormGroup' => '{{label}}',
    // Wrapper container for checkboxes.
    'checkboxWrapper' => '<div>{{label}}</div>',
    // Error message wrapper elements.
    'error' => '<div class="error-message">{{content}}</div>',
    // Container for error items.
    'errorList' => '<ul>{{content}}</ul>',
    // Error item wrapper.
    'errorItem' => '<li>{{text}}</li>',
    // File input used by file().
    'file' => '<input class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500" type="file" name="{{name}}"{{attrs}}>',
    // Fieldset element used by allControls().
    'fieldset' => '<fieldset{{attrs}}>{{content}}</fieldset>',
    // Open tag used by create().
    'formStart' => '<form{{attrs}}>',
    // Close tag used by end().
    'formEnd' => '</form>',
    // General grouping container for control(). Defines input/label ordering.
    'formGroup' => '{{input}}{{label}}',
    // Wrapper content used to hide other content.
    'hiddenBlock' => '<div style="display:none;">{{content}}</div>',
    // Label element when inputs are not nested inside the label.
    'label' => '<label class="absolute tracking-wide py-2 px-4 mb-4 hidden leading-tight top-0 left-0 cursor-text">{{text}}</label>',
    // Label element used for radio and multi-checkbox inputs.
    'nestingLabel' => '{{hidden}}<label{{attrs}} class="inline-flex items-center">{{input}} <span class="ml-2">{{text}}</span></label>',
    // Generic input element.
    'input' => '<input type="{{type}}" class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500" name="{{name}}"{{attrs}} placeholder="Enter {{name}}"/>',
    // Submit input element.
    'inputSubmit' => '<input type="{{type}}"{{attrs}}/>',
    // Container element used by control().
    'inputContainer' => '<div class="relative w-full appearance-none label-floating mb-3 {{type}}{{required}}">{{content}}</div>',
    // Container element used by control() when a field has an error.
    'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
    // Legends created by allControls()
    'legend' => '<legend>{{text}}</legend>',
    // Multi-Checkbox input set title element.
    'multicheckboxTitle' => '<legend>{{text}}</legend>',
    // Multi-Checkbox wrapping container.
    'multicheckboxWrapper' => '<fieldset{{attrs}}>{{content}}</fieldset>',
    // Option element used in select pickers.
    'option' => '<option value="{{value}}"{{attrs}}>{{text}}</option>',
    // Option group element used in select pickers.
    'optgroup' => '<optgroup label="{{label}}"{{attrs}}>{{content}}</optgroup>',
    // Select element,
    'select' => '<select class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500" name="{{name}}"{{attrs}}>{{content}}</select>',
    // Multi-select element,
    'selectMultiple' => '<select name="{{name}}[]" class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500" multiple="multiple"{{attrs}}>{{content}}</select>',
    // Radio input element,
    'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
    // Wrapping container for radio input/label,
    'radioWrapper' => '{{label}}',
    // Textarea input element,
    'textarea' => '<textarea class="tracking-wide py-2 px-4 mb-3 leading-relaxed appearance-none block w-full bg-cool-gray-100 border border-cool-gray-200 rounded focus:outline-none focus:bg-white focus:border-gray-500" name="{{name}}"{{attrs}}>{{value}}</textarea>',
    // Container for submit buttons.
    'submitContainer' => '<div class="submit">{{content}}</div>',
    // Confirm javascript template for postLink()
    'confirmJs' => '{{confirm}}',
    // selected class
    'selectedClass' => 'selected',
];
