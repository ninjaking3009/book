<?php

return [
    'number' => '<a href="{{url}}" class="">{{text}}</a>',
    'nextDisabled ' => '<a href="{{url}}" class="text-xl">{{text}}--</a>',
    'prevDisabled  ' => '<a href="{{url}}" class="text-xl">{{text}}--</a>',
];
