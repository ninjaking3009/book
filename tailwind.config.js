module.exports = {
    purge: [],
    theme: {
        fontFamily: {
            'sans': ['Roboto Slab', 'Sans-serif']
        },
        extend: {},
    },
    variants: {},
    plugins: [
        require('@tailwindcss/ui'),
    ],
}
