-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 18, 2020 at 07:40 AM
-- Server version: 8.0.21
-- PHP Version: 7.3.8

SET SQL_MODE
= "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone
= "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `book`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books`
(
  `id` int NOT NULL,
  `title` varchar
(255) NOT NULL,
  `author` varchar
(255) NOT NULL,
  `user_id` int NOT NULL,
  `status` tinyint
(1) NOT NULL,
  `view_by_standard` tinyint
(1) NOT NULL,
  `view_by_verified` tinyint
(1) NOT NULL,
  `cover` varchar
(255) NOT NULL,
  `amazon_url` varchar
(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`
id`,
`title`,
`author
`, `user_id`, `status`, `view_by_standard`, `view_by_verified`, `cover`, `amazon_url`, `date_added`, `date_updated`) VALUES
(19, 'Low Rates', 'j.k rowling', 1, 1, 0, 1, '1600251799_action-thriller-book-cover-design-template-3675ae3e3ac7ee095fc793ab61b812cc_screen.jpg', 'https://www.amazon.com/Instant-PHP-Scraping-Jacob-Ward-ebook/dp/B00E7NC9CS', '2020-09-16 10:23:19', '2020-09-16 10:23:19'),
(20, 'Office Equipment', 'j.k rowling', 1, 1, 0, 1, '1600255972_a0e626af3883e109da440ec021080f99.jpg', 'https://www.amazon.com/Instant-PHP-Scraping-Jacob-Ward-ebook/dp/B00E7NC9CS', '2020-09-16 11:32:52', '2020-09-16 11:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories`
(
  `id` int NOT NULL,
  `book_id` int NOT NULL,
  `category_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `book_categories`
--

INSERT INTO `book_categories` (`
id`,
`book_id
`, `category_id`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 5, 1),
(4, 5, 2),
(5, 6, 1),
(6, 6, 2),
(7, 7, 1),
(8, 7, 2),
(9, 8, 1),
(10, 8, 2),
(11, 9, 1),
(12, 9, 2),
(13, 10, 1),
(14, 10, 2),
(15, 11, 2),
(23, 12, 1),
(24, 13, 1),
(25, 13, 2),
(26, 16, 1),
(27, 17, 1),
(28, 18, 1),
(29, 19, 1),
(30, 20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `book_question_answer`
--

CREATE TABLE `book_question_answer`
(
  `id` int NOT NULL,
  `book_id` int NOT NULL,
  `question_id` int NOT NULL,
  `answer` text CHARACTER
SET utf8mb4
COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `book_question_answer`
--

INSERT INTO `book_question_answer` (`
id`,
`book_id
`, `question_id`, `answer`) VALUES
(22, 12, 3, 'Best plot|Awesome twist|Unbelievable characters|Superb lesson'),
(23, 12, 4, 'this is summary of good things'),
(24, 12, 5, 'this is book'),
(25, 13, 3, 'Best plot|Superb lesson'),
(26, 13, 4, 'this is summary'),
(27, 13, 5, 'this is test'),
(28, 13, 6, 'First|End'),
(29, 19, 3, 'Best plot|Unbelievable characters'),
(30, 19, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.'),
(31, 19, 5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'),
(32, 19, 6, 'First|End'),
(33, 20, 3, 'Best plot|Unbelievable characters'),
(34, 20, 4, 'lipsum'),
(35, 20, 5, 'lipsum'),
(36, 20, 6, 'Middle');

-- --------------------------------------------------------

--
-- Table structure for table `book_tags`
--

CREATE TABLE `book_tags`
(
  `id` int NOT NULL,
  `book_id` int NOT NULL,
  `name` varchar
(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `book_tags`
--

INSERT INTO `book_tags` (`
id`,
`book_id
`, `name`) VALUES
(1, 8, 'best'),
(2, 8, 'romance'),
(3, 8, 'action'),
(4, 9, 'best'),
(5, 9, 'romance'),
(6, 9, 'action'),
(7, 10, 'best'),
(8, 10, 'romance'),
(9, 10, 'action'),
(10, 11, 'best'),
(11, 11, 'romance'),
(12, 11, 'action'),
(39, 12, 'best'),
(40, 12, 'romance'),
(41, 12, 'action'),
(42, 12, 'plot'),
(43, 13, 'best'),
(44, 13, 'romance'),
(45, 13, 'action'),
(46, 17, 'best'),
(47, 17, 'romance'),
(48, 17, 'action'),
(49, 18, 'best'),
(50, 18, 'romance'),
(51, 18, 'action'),
(52, 19, 'best'),
(53, 19, 'romance'),
(54, 19, 'action'),
(55, 20, 'best'),
(56, 20, 'romance'),
(57, 20, 'action');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories`
(
  `id` int NOT NULL,
  `name` varchar
(255) NOT NULL,
  `status` tinyint
(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`
id`,
`name
`, `status`) VALUES
(1, 'Sci-fi', 1),
(2, 'Drama', 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions`
(
  `id` int NOT NULL,
  `question` text NOT NULL,
  `status` tinyint
(1) NOT NULL,
  `type` varchar
(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`
id`,
`question
`, `status`, `type`) VALUES
(3, 'best thing to describe the book?', 1, 'selection'),
(4, 'Summary', 1, 'text'),
(5, 'What is unique about the book and sets it apart from others?', 0, 'text'),
(6, 'Which part is the best?', 1, 'selection');

-- --------------------------------------------------------

--
-- Table structure for table `question_choices`
--

CREATE TABLE `question_choices`
(
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `name` varchar
(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `question_choices`
--

INSERT INTO `question_choices` (`
id`,
`question_id
`, `name`) VALUES
(15, 3, 'Best plot'),
(16, 3, 'Awesome twist'),
(17, 3, 'Unbelievable characters'),
(18, 3, 'Superb lesson'),
(19, 6, 'First'),
(20, 6, 'Middle'),
(21, 6, 'End');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users`
(
  `id` int NOT NULL,
  `email` varchar
(255) NOT NULL,
  `password` varchar
(255) NOT NULL,
  `first_name` varchar
(255) NOT NULL,
  `last_name` varchar
(255) NOT NULL,
  `status` tinyint
(1) NOT NULL,
  `is_admin` tinyint
(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`
id`,
`email
`, `password`, `first_name`, `last_name`, `status`, `is_admin`, `date_created`) VALUES
(1, 'bryan.sumague09@gmail.com', '$2y$10$2VW4owkZKt2bhiSgPiRQ0ui07nNf82lfNPl02aIEqp1b/BQCud3A.', 'bryan', 'sumague', 0, 0, '2020-09-03 22:59:23'),
(2, 'admin', '$2y$10$2VW4owkZKt2bhiSgPiRQ0ui07nNf82lfNPl02aIEqp1b/BQCud3A.', 'Admin-Ivan', 'Super', 0, 1, '2020-09-03 22:59:23'),
(3, 'john.doe@gmail.com', '$2y$10$NlZ.925uApmfPy9foLKGiekX3ToXFCwXhCsEY/desxFhK3se4Irwi', 'john', 'doe', 1, 0, '2020-09-16 11:54:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `book_question_answer`
--
ALTER TABLE `book_question_answer`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `book_tags`
--
ALTER TABLE `book_tags`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `question_choices`
--
ALTER TABLE `question_choices`
ADD PRIMARY KEY
(`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
ADD PRIMARY KEY
(`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `book_question_answer`
--
ALTER TABLE `book_question_answer`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `book_tags`
--
ALTER TABLE `book_tags`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `question_choices`
--
ALTER TABLE `question_choices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
